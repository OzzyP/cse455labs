package com.example.proel.copycat;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;


public class Lab2 extends AppCompatActivity {

        Button butt;
        EditText in;
        EditText out;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_lab2);

        butt = (Button)findViewById(R.id.button);
        in = (EditText)findViewById(R.id.textView2);
        out = (EditText)findViewById(R.id.textView3);

        butt.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick (View v) {
                String inputValue = in.getText().toString();
                out.setText(inputValue);
            }
        });
    }}

